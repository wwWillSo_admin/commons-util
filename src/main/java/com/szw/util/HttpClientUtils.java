package com.szw.util;

import java.io.IOException;
import java.util.Iterator;
import java.util.Map;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;
import org.apache.log4j.Logger;


public class HttpClientUtils {

	protected static Logger log = Logger.getLogger(HttpClientUtils.class);

	public static String doGet(String url) {
		CloseableHttpClient httpClient = HttpClients.createDefault();
		HttpGet httpget = new HttpGet(url);
		try {

			// 客户端执行get请求 返回响应实体
			HttpResponse response = httpClient.execute(httpget);
			// 获取响应消息实体
			HttpEntity entity = response.getEntity();

			return EntityUtils.toString(entity);
		} catch (ClientProtocolException e) {
			log.error("【httpClient】报错...", e);
		} catch (IOException e) {
			log.error("【httpClient】报错...", e);
		} finally {
			try {
				httpClient.close();
			} catch (IOException e) {
				log.error("【httpClient】报错...", e);
			}
		}
		return null;
	}

	public static String doGet(String url, Map<String, Object> params) {
		CloseableHttpClient httpClient = HttpClients.createDefault();
		HttpGet httpget = new HttpGet(assembleParams(url, params));
		try {
			// 客户端执行get请求 返回响应实体
			HttpResponse response = httpClient.execute(httpget);
			// 获取响应消息实体
			HttpEntity entity = response.getEntity();
			return EntityUtils.toString(entity);
		} catch (ClientProtocolException e) {
			log.error("【httpClient】报错...", e);
		} catch (IOException e) {
			log.error("【httpClient】报错...", e);
		} finally {
			try {
				httpClient.close();
			} catch (IOException e) {
				log.error("【httpClient】报错...", e);
			}
		}
		return null;
	}

	private static String assembleParams(String url, Map<String, Object> params) {
		StringBuilder strBuf = new StringBuilder(url);
		if (params.size() > 0) {
			strBuf.append("?");
			Iterator<Map.Entry<String, Object>> iter = params.entrySet().iterator();
			while (iter.hasNext()) {
				Map.Entry<String, Object> entry = iter.next();
				strBuf.append(entry.getKey() + "=" + entry.getValue());
				if (iter.hasNext()) {
					strBuf.append("&");
				}
			}
		}
		return strBuf.toString();
	}

	public static String doPost(String url, String json) {
		CloseableHttpClient httpClient = HttpClients.createDefault();
		HttpPost httpPost = new HttpPost(url);

		try {

			StringEntity s = new StringEntity(json);
			s.setContentEncoding("UTF-8");
			s.setContentType("application/json");// 发送json数据需要设置contentType
			httpPost.setEntity(s);

			// 客户端执行get请求 返回响应实体
			HttpResponse response = httpClient.execute(httpPost);
			// 获取响应消息实体
			HttpEntity entity = response.getEntity();

			return EntityUtils.toString(entity);
		} catch (ClientProtocolException e) {
			log.error("【httpClient】报错...", e);
		} catch (IOException e) {
			log.error("【httpClient】报错...", e);
		} finally {
			try {
				httpClient.close();
			} catch (IOException e) {
				log.error("【httpClient】报错...", e);
			}
		}
		return null;
	}
}
