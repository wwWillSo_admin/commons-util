/*******************************************************************************
 * Project Key : lif-financing-common
 * Create on 2015-7-24 下午5:44:03 
 * Copyright (c) 2004 - 2015. 北京拉卡拉支付有限公司版权所有. 京ICP备12007612号
 * 注意：本内容仅限于北京拉卡拉支付有限公司内部传阅，禁止外泄以及用于其他的商业目的
 ******************************************************************************/
package com.szw.util;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


/**
 * 
 * <P>>对象转成Map工具类</P>
 * 
 * @author 苏镇威 2018年8月14日 上午10:06:15
 */
public class ObjectToMapUtils {

	private static final String[] FIX_IGNOREP_ROPERTY = { "serialVersionUID" };

	/**
	 * 
	 * <p>将目标对象的所有属性转换成Map对象</p>
	 * 
	 * @param target
	 * @return
	 * @author 苏镇威 2018年8月14日 上午10:06:23
	 */
	public static <T> Map<String, T> toMap(Object target) {
		return toMap(target, false);
	}

	/**
	 * 
	 * <p>将目标对象的所有属性转换成Map对象</p>
	 * 
	 * @param target
	 * @param ignoreParent
	 * @return
	 * @author 苏镇威 2018年8月14日 上午10:06:31
	 */
	public static <T> Map<String, T> toMap(Object target, boolean ignoreParent) {
		return toMap(target, ignoreParent, false);
	}

	/**
	 * 
	 * <p>将目标对象的所有属性转换成Map对象</p>
	 * 
	 * @param target
	 * @param ignoreParent
	 * @param ignoreEmptyValue
	 * @return
	 * @author 苏镇威 2018年8月14日 上午10:06:38
	 */
	public static <T> Map<String, T> toMap(Object target, boolean ignoreParent, boolean ignoreEmptyValue) {
		return toMap(target, ignoreParent, ignoreEmptyValue, new String[0]);
	}

	/**
	 * 
	 * <p>将目标对象的所有属性转换成Map对象</p>
	 * 
	 * @param target
	 * @param ignoreParent
	 * @param ignoreEmptyValue
	 * @param ignoreProperties
	 * @return
	 * @author 苏镇威 2018年8月14日 上午10:06:45
	 */
	public static <T> Map<String, T> toMap(Object target, boolean ignoreParent, boolean ignoreEmptyValue, String... ignoreProperties) {
		final Map<String, T> map = new HashMap<String, T>();
		if (target == null) {
			return map;
		}
		final Map<String, String> ignoreMap = getIgnorePropertiesMap(ignoreProperties);
		final List<Field> fields = getClassFields(ignoreParent, target.getClass());
		for (Field field : fields) {
			T value = getFieldsValue(target, field);
			if (isIgnoreEmpty(value, ignoreEmptyValue) || isIgnoreProperty(field.getName(), ignoreMap)) {
				continue;
			}
			map.put(field.getName(), value);
		}
		return map;
	}

	/**
	 * 
	 * <p>获得忽略的属性Map条件</p>
	 * 
	 * @param ignoreProperties
	 * @return
	 * @author 苏镇威 2018年8月14日 上午10:06:52
	 */
	public static Map<String, String> getIgnorePropertiesMap(final String[] ignoreProperties) {
		final Map<String, String> resultMap = new HashMap<String, String>();
		for (String ignoreProperty : FIX_IGNOREP_ROPERTY) {
			resultMap.put(ignoreProperty, null);
		}
		for (String ignoreProperty : ignoreProperties) {
			resultMap.put(ignoreProperty, null);
		}
		return resultMap;
	}

	/**
	 * 
	 * <p>获取类实例的属性</p>
	 * 
	 * @param ignoreParent
	 * @param clazz
	 * @return
	 * @author 苏镇威 2018年8月14日 上午10:06:59
	 */
	public static List<Field> getClassFields(boolean ignoreParent, Class<?> clazz) {
		final List<Field> fields = new ArrayList<Field>();
		fields.addAll(Arrays.asList(clazz.getDeclaredFields()));
		if (!ignoreParent) {
			fields.addAll(getParentClassFields(fields, clazz.getSuperclass()));
		}
		return fields;
	}

	/**
	 * 
	 * <p>获取类实例的父类的属性</p>
	 * 
	 * @param fields
	 * @param clazz
	 * @return
	 * @author 苏镇威 2018年8月14日 上午10:07:05
	 */
	private static List<Field> getParentClassFields(List<Field> fields, Class<?> clazz) {
		fields.addAll(Arrays.asList(clazz.getDeclaredFields()));
		if (clazz.getSuperclass() == null) {
			return fields;
		}
		return getParentClassFields(fields, clazz.getSuperclass());
	}

	/**
	 * 
	 * <p>获取类实例的属性值</p>
	 * 
	 * @param target
	 * @param field
	 * @return
	 * @author 苏镇威 2018年8月14日 上午10:07:12
	 */
	@SuppressWarnings("unchecked")
	public static <T> T getFieldsValue(Object target, Field field) {
		field.setAccessible(true);
		T value = null;
		try {
			value = (T) field.get(target);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return value;
	}

	/**
	 * 
	 * <p>是否忽略属性</p>
	 * 
	 * @param fieldName
	 * @param ignoreMap
	 * @return
	 * @author 苏镇威 2018年8月14日 上午10:07:19
	 */
	public static <T> boolean isIgnoreProperty(String fieldName, final Map<String, String> ignoreMap) {
		return ignoreMap.containsKey(fieldName);
	}

	/**
	 * 
	 * <p>是否忽略空值</p>
	 * 
	 * @param value
	 * @param ignoreEmptyValue
	 * @return
	 * @author 苏镇威 2018年8月14日 上午10:07:27
	 */
	public static <T> boolean isIgnoreEmpty(T value, boolean ignoreEmptyValue) {
		if (!ignoreEmptyValue) {
			return false;
		}
		boolean isEmpty = (value == null || value.toString().equals(""));
		boolean isCollectionEmpty = (value instanceof Collection && ((Collection<?>) value).isEmpty());
		boolean isMapEmpty = (value instanceof Map && ((Map<?, ?>) value).isEmpty());
		return (isEmpty || isCollectionEmpty || isMapEmpty);
	}

}
