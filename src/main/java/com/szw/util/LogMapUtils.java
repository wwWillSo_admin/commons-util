package com.szw.util;

import java.lang.reflect.Field;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import com.szw.annotation.LogFilterField;


/**
 * 
 * <P>对象转换Map做日志输出的工具类</P>
 * 
 * @author 苏镇威 2018年8月14日 上午10:05:07
 */
public class LogMapUtils {

	/**
	 * 
	 * <p>将目标对象的所有属性转换成Map对象</p>
	 * 
	 * @param target
	 * @return
	 * @author 苏镇威 2018年8月14日 上午10:05:19
	 */
	public static Map<String, Object> toLogMap(Object target) {
		return toLogMap(target, false);
	}

	/**
	 * 
	 * <p>将目标对象的所有属性转换成Map对象</p>
	 * 
	 * @param target
	 * @param ignoreParent
	 * @return
	 * @author 苏镇威 2018年8月14日 上午10:05:37
	 */
	public static Map<String, Object> toLogMap(Object target, boolean ignoreParent) {
		return toLogMap(target, ignoreParent, false);
	}

	/**
	 * 
	 * <p>将目标对象的所有属性转换成Map对象</p>
	 * 
	 * @param target
	 * @param ignoreParent
	 * @param ignoreEmptyValue
	 * @return
	 * @author 苏镇威 2018年8月14日 上午10:05:51
	 */
	public static Map<String, Object> toLogMap(Object target, boolean ignoreParent, boolean ignoreEmptyValue) {
		return toLogMap(target, ignoreParent, ignoreEmptyValue, new String[0]);
	}

	/**
	 * 
	 * <p>将目标对象的所有属性转换成Map对象</p>
	 * 
	 * @param target
	 * @param ignoreParent
	 * @param ignoreEmptyValue
	 * @param ignoreProperties
	 * @return
	 * @author 苏镇威 2018年8月14日 上午10:06:00
	 */
	public static Map<String, Object> toLogMap(Object target, boolean ignoreParent, boolean ignoreEmptyValue, String... ignoreProperties) {
		final Map<String, Object> map = new LinkedHashMap<String, Object>();
		if (target == null) {
			return map;
		}
		final Map<String, String> ignoreMap = ObjectToMapUtils.getIgnorePropertiesMap(ignoreProperties);
		final List<Field> fields = ObjectToMapUtils.getClassFields(ignoreParent, target.getClass());
		for (Field field : fields) {
			Object value = ObjectToMapUtils.getFieldsValue(target, field);
			if (ObjectToMapUtils.isIgnoreEmpty(value, ignoreEmptyValue) || ObjectToMapUtils.isIgnoreProperty(field.getName(), ignoreMap)) {
				continue;
			}
			// 有标识Log输出类型的
			LogFilterField logField = field.getAnnotation(LogFilterField.class);
			if (logField == null) {
				map.put(field.getName(), value);
				continue;
			}
			if (LogFilterField.IgnoreType.PROPERTY.equals(logField.ignore())) {
				continue;
			}
			if (LogFilterField.IgnoreType.DEFAULT.equals(logField.ignore())) {
				map.put(field.getName(), null);
			} else if (LogFilterField.IgnoreType.VALUE_REPLACE.equals(logField.ignore())) {
				map.put(field.getName(), logField.message());
			} else {
				map.put(field.getName(), value);
			}
		}
		return map;
	}

}
