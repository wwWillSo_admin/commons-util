package com.szw.annotation;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Inherited;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;


/**
 * 
 * <P>此注解用于输入日志时是否需要过滤某些敏感字段</P>
 * 
 * @author 苏镇威 2018年8月14日 上午10:02:47
 */
@Target({ ElementType.FIELD })
@Retention(RetentionPolicy.RUNTIME)
@Inherited
@Documented
public @interface LogFilterField {

	enum IgnoreType {
		/** 忽略属性 */
		PROPERTY,
		/** 忽略值 */
		DEFAULT,
		/** 忽略值，允许替换信息 */
		VALUE_REPLACE,
		/** 不忽略 */
		NO;
	}

	/** 忽略属性，默认为忽略属性IgnoreType.DEFAULT **/
	IgnoreType ignore() default IgnoreType.DEFAULT;

	/** 替代信息，注意：IgnoreType.VALUE_REPLACE，此方法才生效 **/
	String message() default "";

}
